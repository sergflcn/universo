extends KinematicBody

const SPEED = 300


func _physics_process(delta):
	var dir = Vector3()
	
	if Input.is_action_pressed("ui_left"):
		rotate_object_local(Vector3(0, 0, 1), -delta)
	if Input.is_action_pressed("ui_right"):
		rotate_object_local(Vector3(0, 0, 1), delta)
	if Input.is_action_pressed("ui_down"):
		rotate_object_local(Vector3(1, 0, 0), delta)
	if Input.is_action_pressed("ui_up"):
		rotate_object_local(Vector3(1, 0, 0), -delta)
	if Input.is_action_pressed("ui_page_up"):
		dir += transform.basis.z.normalized()
	if Input.is_action_pressed("ui_page_down"):
		dir -= transform.basis.z.normalized()
		
	if dir:
		dir *= SPEED * delta
		move_and_slide(dir, Vector3(0, 0, 1))
